﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Newtonsoft.Json;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Xml;
using System.Xml.Linq;
//using WeatherApp.DAL;
using System.Web;

namespace Platformy_Programistyczne_Kalendarz
{
    public partial class Weather : Form
    {

        const string APPID = "997980fa48c7252efc75b6aad2cb1b05";
        string cityName = "Wroclaw";
        public Weather()
        {
            InitializeComponent();
            getWeather();
            getForecast();
        }

        public const string sunny = "https://images.vexels.com/media/users/3/145138/isolated/preview/1de68bf333344dc5a6efca43807cfc6d-sun-small-line-beams-icon-by-vexels.png";
        public const string snowy = "http://files.softicons.com/download/web-icons/vector-stylish-weather-icons-by-bartosz-kaszubowski/png/256x256/cloud.snow.png";
        public const string cloudy = "https://pl.seaicons.com/wp-content/uploads/2015/06/Cloud-icon3.png";
        public const string rainy = "http://files.softicons.com/download/web-icons/vector-stylish-weather-icons-by-bartosz-kaszubowski/png/256x256/cloud.rain.png";

        void getWeather()
        {
            using (WebClient web = new WebClient())
            {
                string url = string.Format("http://api.openweathermap.org/data/2.5/weather?q=Wroclaw&appid=997980fa48c7252efc75b6aad2cb1b05&units=metric&cnt=6");
                var json = web.DownloadString(url);
                var result = JsonConvert.DeserializeObject<weatherInfo.root>(json);
                weatherInfo.root outPut = result;
                //label1.Text = string.Format("{0}", outPut.name);
                //label2.Text = string.Format("{0}", outPut.sys.country);
                label3.Text = string.Format("{0} \u00B0" + "C", outPut.main.temp);
                label10.Text = string.Format("{0} km/h", outPut.wind.speed);
                label11.Text = string.Format("{0} hpa", outPut.main.pressure);
                label13.Text = string.Format("{0} %", outPut.main.humidity);

                if (outPut.main.temp<0) { pictureBox1.ImageLocation = snowy; }
                else if (outPut.main.humidity>=75) { pictureBox1.ImageLocation = rainy; }
                else if (outPut.main.humidity < 75 && outPut.main.humidity >= 25) { pictureBox1.ImageLocation = cloudy; }
                else if (outPut.main.humidity < 25) { pictureBox1.ImageLocation = sunny; }


            }
        }
        void getForecast()
        {
            string url2 = string.Format("http://api.openweathermap.org/data/2.5/forecast?q=Wroclaw&appid=997980fa48c7252efc75b6aad2cb1b05");
            using (WebClient web = new WebClient())
            {
                var json = web.DownloadString(url2);
                var Object = JsonConvert.DeserializeObject<weatherForecast>(json);
                weatherForecast forecast = Object;

                label8.Text = string.Format("{0}", GetDate(forecast.list[7].dt).DayOfWeek);
                label2.Text = string.Format("{0}", forecast.list[7].weather[0].main);
                label5.Text = string.Format("{0}", forecast.list[7].weather[0].description);
                label6.Text = string.Format("{0}\u00B0"+"C", forecast.list[7].main.temp-273);
                label7.Text = string.Format("{0} km/h", forecast.list[7].wind.speed);
                if (forecast.list[7].weather[0].main == "Snow") { pictureBox2.ImageLocation = snowy; }
                else if (forecast.list[7].weather[0].main == "Rain") { pictureBox2.ImageLocation = rainy; }
                else if (forecast.list[7].weather[0].main == "Clouds") { pictureBox2.ImageLocation = cloudy; }
                else if (forecast.list[7].weather[0].main == "Clear") { pictureBox2.ImageLocation = sunny; }

                label21.Text = string.Format("{0}", GetDate(forecast.list[15].dt).DayOfWeek);
                label17.Text = string.Format("{0}", forecast.list[15].weather[0].main);
                label18.Text = string.Format("{0}", forecast.list[15].weather[0].description);
                label19.Text = string.Format("{0}\u00B0" + "C", forecast.list[15].main.temp-273);
                label20.Text = string.Format("{0} km/h", forecast.list[15].wind.speed);
                if (forecast.list[15].weather[0].main == "Snow") { pictureBox3.ImageLocation = snowy; }
                else if (forecast.list[15].weather[0].main == "Rain") { pictureBox3.ImageLocation = rainy; }
                else if (forecast.list[15].weather[0].main == "Clouds") { pictureBox3.ImageLocation = cloudy; }
                else if (forecast.list[15].weather[0].main == "Clear") { pictureBox3.ImageLocation = sunny; }

                label27.Text = string.Format("{0}", GetDate(forecast.list[23].dt).DayOfWeek);
                label23.Text = string.Format("{0}", forecast.list[23].weather[0].main);
                label24.Text = string.Format("{0}", forecast.list[23].weather[0].description);
                label25.Text = string.Format("{0}\u00B0" + "C", forecast.list[23].main.temp-273);
                label26.Text = string.Format("{0} km/h", forecast.list[23].wind.speed);
                if (forecast.list[23].weather[0].main == "Snow") { pictureBox4.ImageLocation = snowy; }
                else if (forecast.list[23].weather[0].main == "Rain") { pictureBox4.ImageLocation = rainy; }
                else if (forecast.list[23].weather[0].main == "Clouds") { pictureBox4.ImageLocation = cloudy; }
                else if (forecast.list[23].weather[0].main == "Clear") { pictureBox4.ImageLocation = sunny; }

                label33.Text = string.Format("{0}", GetDate(forecast.list[31].dt).DayOfWeek);
                label29.Text = string.Format("{0}", forecast.list[31].weather[0].main);
                label30.Text = string.Format("{0}", forecast.list[31].weather[0].description);
                label31.Text = string.Format("{0}\u00B0" + "C", forecast.list[31].main.temp-273);
                label32.Text = string.Format("{0} km/h", forecast.list[31].wind.speed);
                if (forecast.list[31].weather[0].main == "Snow") { pictureBox5.ImageLocation = snowy; }
                else if (forecast.list[31].weather[0].main == "Rain") { pictureBox5.ImageLocation = rainy; }
                else if (forecast.list[31].weather[0].main == "Clouds") { pictureBox5.ImageLocation = cloudy; }
                else if (forecast.list[31].weather[0].main == "Clear") { pictureBox5.ImageLocation = sunny; }
            }
        }

        DateTime GetDate(double millisecound)
        {
            DateTime day = new System.DateTime(1970, 1, 1, 0, 0, 0, System.DateTimeKind.Utc).ToLocalTime();
            day = day.AddSeconds(millisecound).ToLocalTime();
            return day;
        }
        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void label3_Click(object sender, EventArgs e)
        {

        }

        private void label4_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }
    }
}
