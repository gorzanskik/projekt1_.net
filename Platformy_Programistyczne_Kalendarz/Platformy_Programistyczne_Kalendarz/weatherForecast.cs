﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformy_Programistyczne_Kalendarz
{
    public class weatherForecast
    {
        public city city { get; set; }
        public List<list> list { get; set; }//foreacst list
    }

    public class temp
    {
        public double day { get; set; }
    }
    public class weather
    {
        public string main { get; set; }
        public string description { get; set; }
    }

    public class city
    {
        public string name { get; set; }
    }

    public class main
    {
        public double temp { get; set; }
    }

    public class wind
    {
        public double speed { get; set; }
    }

    public class list
    {
        public double dt { get; set; }//dzien w milisekundach
        public double pressure { get; set; }
        public double humidity { get; set; }
        public double speed { get; set; }
        public temp temp { get; set; }
        public wind wind { get; set; }
        public main main { get; set; }
        public List<weather> weather { get; set; }//weather list
    }
}
