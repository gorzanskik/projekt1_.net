﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Security.Cryptography.X509Certificates;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace Platformy_Programistyczne_Kalendarz
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        ObservableCollection<Events> List = new ObservableCollection<Events>();
        ObservableCollection<Events> HelpList = new ObservableCollection<Events>();
        public MainWindow()
        {
            InitializeComponent();

            ListOfEvents.ItemsSource = List;

        }

        private void Calendar_SelectedDatesChanged(object sender, SelectionChangedEventArgs e)
        {
            Calendar calendar = sender as Calendar;

            DateTime? selectedDate = calendar.SelectedDate;
            DateTime onlyDate = selectedDate.Value;

            BigOnce.DisplayDate = onlyDate; //Wiekszy kalendarz
        }

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void ToggleButton_Checked(object sender, RoutedEventArgs e)
        {
            bool isnumber1 = false, isnumber2 = false, isnumber3 = false;
            bool isnumberinsert = false;

            void IsNumber(string text, ref bool number)
            {
                for (int i = 0; i < text.Length; i++)
                {
                    if (text[i] == '0' || text[i] == '1' || text[i] == '2' || text[i] == '3' || text[i] == '4' || text[i] == '5' || text[i] == '6' || text[i] == '7' || text[i] == '8' || text[i] == '9')

                        number = true;

                    else

                        number = false;
                }
            }

            string d = EventDay.Text;
            string m = EventMonth.Text;
            string y = EventYear.Text;
            string n = InsertNumber.Text;
            IsNumber(d, ref isnumber1);
            IsNumber(m, ref isnumber2);
            IsNumber(y, ref isnumber3);
            IsNumber(n, ref isnumberinsert);

            if (d == "" || m == "" || y == "" || EventName.Text == "" || EventDescription.Text == "")
                MessageBox.Show("Podane dane są niepełne.", "Uwaga!");
            else
            {
                if (isnumber1 == true && isnumber2 == true && isnumber3 == true)
                {

                    int day = Convert.ToInt32(d);
                    int month = Convert.ToInt32(m);
                    int year = Convert.ToInt32(y);

                    if (InsertNumber.Text == "")
                        List.Add(new Events(day, month, year, EventName.Text, EventDescription.Text));

                    else
                    {
                        if (isnumberinsert == true)     //nie od zera tylko od 1
                        {
                            int insertnumber = Convert.ToInt32(InsertNumber.Text);
                            List.Insert(insertnumber - 1, new Events(day, month, year, EventName.Text, EventDescription.Text));
                        }
                        else
                            MessageBox.Show("Niepoprawnie wprowadzone miejsce", "Uwaga!");
                    }
                }

                else
                    MessageBox.Show("Niepoprawnie wprowadzona data!", "Uwaga!");
            }

            EventDay.Text = "";
            EventMonth.Text = "";
            EventYear.Text = "";
            EventName.Text = "";
            EventDescription.Text = "";
            InsertNumber.Text = "";


            AddEvent.IsChecked = false;
            Accept.IsChecked = false;
        }

        private void ToggleButton_Checked_1(object sender, RoutedEventArgs e)
        {
            int SelectedNameIndex = ListOfEvents.SelectedIndex; //wybieranie po indeksie czyli numerze elementu

            if (SelectedNameIndex != -1)
                List.RemoveAt(SelectedNameIndex);
            else
                MessageBox.Show("Lista nie zawiera żadnych wydarzeń lub wydarzenie nie jest zaznaczone.", "Uwaga!");
            RemoveEvent.IsChecked = false;
        }

        private void ToggleButton_Checked_2(object sender, RoutedEventArgs e)
        {
            List.Add(new Events(1, 3, 2020, "Narodowy Dzień Pamięci „Żołnierzy Wyklętych”", "Święto państwowe"));
            List.Add(new Events(1, 5, 2020, "Święto Pracy", "Święto państwowe"));
            List.Add(new Events(3, 5, 2020, "Święto Narodowe Trzeciego Maja", "Święto państwowe"));
            List.Add(new Events(8, 5, 2020, "Narodowe Święto Zwycięstwa i Wolności", "Święto państwowe"));
            List.Add(new Events(1, 8, 2020, "Narodowy Dzień Pamięci Powstania Warszawskiego", "Święto państwowe"));
            List.Add(new Events(31, 8, 2020, "Dzień Solidarności i Wolności", "Święto państwowe"));
            List.Add(new Events(11, 11, 2020, "Narodowe Święto Niepodległości", "Święto państwowe"));

            AddPolish.IsChecked = false;
        }

        private void ToggleButton_Checked_3(object sender, RoutedEventArgs e)
        {
            List.Remove(new Events() { eventname = "Narodowy Dzień Pamięci „Żołnierzy Wyklętych”" });
            List.Remove(new Events() { eventname = "Święto Pracy" });
            List.Remove(new Events() { eventname = "Święto Narodowe Trzeciego Maja" });
            List.Remove(new Events() { eventname = "Narodowe Święto Zwycięstwa i Wolności" });
            List.Remove(new Events() { eventname = "Narodowy Dzień Pamięci Powstania Warszawskiego" });
            List.Remove(new Events() { eventname = "Dzień Solidarności i Wolności" });
            List.Remove(new Events() { eventname = "Narodowe Święto Niepodległości" });

            RemovePolish.IsChecked = false;
        }

        private void ToggleButton_Checked_4(object sender, RoutedEventArgs e)
        {
            //Linq
            /*
            var myList = List
                .OrderBy(s => s.day)
                .ThenBy(s => s.month)
                .ThenBy(s => s.year);
            */

            if (List.Count == 0)
                MessageBox.Show("Lista jest pusta!", "Uwaga!");
            else
            {

                ListOfEvents.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription("year",
                System.ComponentModel.ListSortDirection.Ascending));

                ListOfEvents.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription("month",
                System.ComponentModel.ListSortDirection.Ascending));

                ListOfEvents.Items.SortDescriptions.Add(
                new System.ComponentModel.SortDescription("day",
                System.ComponentModel.ListSortDirection.Ascending));


            }

            SortDate.IsChecked = false;
        }

        private void ToggleButton_Checked_5(object sender, RoutedEventArgs e)
        {
            int b = 0;

            for (int i = 0; i < List.Count; i++)
            {
                if (RemoveName.Text == List[i].eventname)
                    b = 1;
            }

            if (List.Count == 0)
                MessageBox.Show("Lista jest pusta!", "Uwaga!");

            else if (RemoveName.Text == "")
                MessageBox.Show("Wprowadź nazwę wydarzenia!", "Uwaga!");

            else if (b == 0)
                MessageBox.Show("Wydarzenie o podanej nazwie nie istnieje!", "Uwaga!");

            else
            {
                List.Remove(new Events() { eventname = RemoveName.Text });
                RemoveName.Text = "";
            }

            RemoveButton.IsChecked = false;
        }

        public void IsNumber(string text, ref bool number)
        {
            for (int i = 0; i < text.Length; i++)
            {
                if (text[i] == '0' || text[i] == '1' || text[i] == '2' || text[i] == '3' || text[i] == '4' || text[i] == '5' || text[i] == '6' || text[i] == '7' || text[i] == '8' || text[i] == '9')

                    number = true;

                else

                    number = false;
            }
        }

        private void ByDay_Checked(object sender, RoutedEventArgs e)
        {
            if (SortDay.Text == "")
                MessageBox.Show("Pole nie jest wypełnione", "Uwaga!");
            else
            {
                bool isnumber = false;
                string text = SortDay.Text;
                IsNumber(text, ref isnumber);

                if (isnumber == true)
                {
                    int number = Convert.ToInt32(text);
                    for (int i = 0; i < List.Count; i++)
                    {
                        if (List[i].day == number)
                            HelpList.Add(List[i]);
                    }
                }

            }

            ListOfEvents.ItemsSource = HelpList;
            ByDay.IsChecked = false;
        }

        private void ByMonth_Checked(object sender, RoutedEventArgs e)
        {
            if (SortMonth.Text == "")
                MessageBox.Show("Pole nie jest wypełnione", "Uwaga!");
            else
            {
                bool isnumber = false;
                string text = SortMonth.Text;
                IsNumber(text, ref isnumber);

                if (isnumber == true)
                {
                    int number = Convert.ToInt32(text);
                    for (int i = 0; i < List.Count; i++)
                    {
                        if (List[i].month == number)
                            HelpList.Add(List[i]);
                    }
                }

            }
            ListOfEvents.ItemsSource = HelpList;
            ByMonth.IsChecked = false;
        }

        private void ByYear_Checked(object sender, RoutedEventArgs e)
        {
            if (SortYear.Text == "")
                MessageBox.Show("Pole nie jest wypełnione", "Uwaga!");
            else
            {
                bool isnumber = false;
                string text = SortYear.Text;
                IsNumber(text, ref isnumber);

                if (isnumber == true)
                {
                    int number = Convert.ToInt32(text);
                    for (int i = 0; i < List.Count; i++)
                    {
                        if (List[i].year == number)
                            HelpList.Add(List[i]);
                    }
                }

            }
            ListOfEvents.ItemsSource = HelpList;
            ByYear.IsChecked = false;
        }

        private void ToggleButton_Checked_Pogoda(object sender, RoutedEventArgs e) 
        { 
            Weather settingsForm = new Weather(); 
            settingsForm.Show();

            Weather.IsChecked = false;
        }

        private void ByName_Checked(object sender, RoutedEventArgs e)
        {
            if (SortYear.Text == "")
                MessageBox.Show("Pole nie jest wypełnione", "Uwaga!");
            else
            {
                string text = SortYear.Text;

                for (int i = 0; i < List.Count; i++)
                {
                    if (List[i].eventname == text)
                        HelpList.Add(List[i]);
                }


            }
            ListOfEvents.ItemsSource = HelpList;
            ByName.IsChecked = false;
        }

        private void ToggleButton_Checked_6(object sender, RoutedEventArgs e)
        {
            ListOfEvents.ItemsSource = List;
            HelpList.Clear();
            SortDay.Text = "";
            SortMonth.Text = "";
            SortYear.Text = "";
            SortName.Text = "";
            Return.IsChecked = false;
        }
    }
}
