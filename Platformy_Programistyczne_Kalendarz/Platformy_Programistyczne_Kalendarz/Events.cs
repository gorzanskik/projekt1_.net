﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Platformy_Programistyczne_Kalendarz
{
    public class Events : INotifyPropertyChanged
    {
        private DateTime Date;
        private int Day;
        private int Month;
        private int Year;
        private string EventName;
        private string EventDescription;

        public string EventDate;

        //add

        public int day { get { return Day; } set { Day = value; OnPropertyRaised("day"); } }

        public int month { get { return Month; } set { Month = value; OnPropertyRaised("month"); } }

        public int year { get { return Year; } set { Year = value; OnPropertyRaised("year"); } }

        public string eventname { get { return EventName; } set { EventName = value; OnPropertyRaised("eventname"); } }

        public string eventdescription { get { return EventDescription; } set { EventDescription = value; OnPropertyRaised("eventdescription"); } }

        public Events(int day, int month, int year, string eventname, string eventdescription)
        {
            this.day = day;
            this.month = month;
            this.year = year;
            this.eventname = eventname;
            this.eventdescription = eventdescription;

            string d = Convert.ToString(day);
            string m = Convert.ToString(month);
            string y = Convert.ToString(year);

            EventDate = d + "/" + m + "/" + y;

            DateTime date = new DateTime(this.year, this.month, this.day);
            Date = date;
        }

        public Events()
        {

        }

        public override string ToString()
        {
            return EventDate + ", " + eventname + ", " + eventdescription;
        }

        private void OnPropertyRaised(string name)
        {
            if (PropertyChanged != null)
            {
                PropertyChanged(this, new PropertyChangedEventArgs(name));
            }

        }

        public event PropertyChangedEventHandler PropertyChanged;

        public bool Equals(Events other)
        {
            if (other == null)
                return false;
            return (this.eventname.Equals(other.eventname));
        }

        public override bool Equals(object obj)
        {
            if (obj == null)
                return false;
            Events objAsPart = obj as Events;
            if (objAsPart == null)
                return false;
            else
                return Equals(objAsPart);
        }
    }
}
